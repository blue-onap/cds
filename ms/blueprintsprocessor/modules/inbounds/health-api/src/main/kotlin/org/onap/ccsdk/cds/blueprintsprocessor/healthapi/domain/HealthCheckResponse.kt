package org.onap.ccsdk.cds.blueprintsprocessor.healthapi.domain


data class HealthCheckResponse(val name: String, val status: HealthCheckStatus)


