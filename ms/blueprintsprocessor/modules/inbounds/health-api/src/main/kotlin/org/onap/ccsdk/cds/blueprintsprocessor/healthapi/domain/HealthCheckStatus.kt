package org.onap.ccsdk.cds.blueprintsprocessor.healthapi.domain

enum class HealthCheckStatus {
    UP,
    DOWN
}