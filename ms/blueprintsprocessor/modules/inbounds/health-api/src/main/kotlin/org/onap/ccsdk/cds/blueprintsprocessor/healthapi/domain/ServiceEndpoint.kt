package org.onap.ccsdk.cds.blueprintsprocessor.healthapi.domain



data class ServiceEndpoint(val serviceName: String, val serviceLink: String)
